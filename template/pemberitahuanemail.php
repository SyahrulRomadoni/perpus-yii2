<?php

use yii\helpers\Url;
use yii\helpers\Html;

?>
<div class="row">
	<center>
		<img src="https://yt3.ggpht.com/a-/AN66SAxRHox4MJumwFvuzcxkn59YF-jIV8Bh6Gb3pQ=s900-mo-c-c0xffffffff-rj-k-no" style="width: 250px; height: 250px;">
		<hr width="60%">
		<h3>Kepada Yang Terhormat.</h3>
		<h1><?= $model->anggota->nama ?></h1>
		<p>Anda Telah Meminjam Buku yakni :</p>
		<hr width="60%">
		<p>Nama Buku : <h3><?= $model->buku->nama ?></h3></p>
		<p>Tanggal Pinjam : <h3><?= $model->tanggal_pinjam ?></h3></p>
		<p>Tanggal Kembali : <h3><?= $model->tanggal_kembali ?></h3></p>
		<hr width="60%">
	</center>
</div>