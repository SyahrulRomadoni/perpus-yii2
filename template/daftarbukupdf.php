<?php
	use yii\helpers\Html;
	use app\components\Helper;
?>
<h1>DAFTAR BUKU</h1>
<h1>Daftar Buku Perpustakaan Yii2</h1>
<table class="table-pdf" style="margin:auto; width:100%;">
	<thead>
		<tr>
			<th><?= strtoupper("Nama") ?></th>
			<th><?= strtoupper("Tahun Terbit") ?></th>
			<th><?= strtoupper("Penulis") ?></th>
			<th><?= strtoupper("Penerbit") ?></th>
			<th><?= strtoupper("Kategori") ?></th>
			<th><?= strtoupper("Harga") ?></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($model as $data) { ?>
		<tr>
			<td><?= $data->nama ?></td>
			<td><?= $data->tahun_terbit ?></td>
			<td><?= $data->penulis->nama ?></td>
			<td><?= $data->penerbit->nama ?></td>
			<td><?= $data->kategori->nama ?></td>
			<td><?= $data->harga ?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>
<br>
<h4>Jumlah total harga Buku : Rp. <?= $data->getJumlahHarga(); ?></h4>