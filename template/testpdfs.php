<?php
	use yii\helpers\Html;
	use app\components\Helper;
?>

<div class="garis jarak">

	<p class="tengah line">PERHITUNGAN DAN NOTA TAGIHAN JASA PELAYANAN KAPELABUHANAN</p><br>
	<p class="tengah">Nomer : 04/BK-PLT2/PNT/X/2018</p>

	<p style="font-size: 10px">Kepada :</p>

	<table class="table">
		<tr>
			<td width="45px">1.</td>
			<td width="200px">Nama Pemohon/Agent</td>
			<td width="5px">:</td>
			<td width="255px" class="garis-hitam"></td>
			<td width="250"></td>
		</tr>
		<tr>
			<td width="45px">2.</td>
			<td width="200px">No. NPWRD</td>
			<td width="5px">:</td>
			<td width="255px" class="garis-hitam"></td>
			<td width="250"></td>
		</tr>
		<tr>
			<td width="45px">3.</td>
			<td width="200px">Pemilik/Agent</td>
			<td width="5px">:</td>
			<td width="255px" class="garis-hitam"></td>
			<td width="250"></td>
		</tr>
		<tr>
			<td width="45px">4.</td>
			<td width="200px">Nama Kapal</td>
			<td width="5px">:</td>
			<td width="255px" class="garis-hitam"></td>
			<td width="250"></td>
		</tr>
		<tr>
			<td width="45px">5.</td>
			<td width="200px">Bendera/Kebangsaan</td>
			<td width="5px">:</td>
			<td width="255px" class="garis-hitam"></td>
			<td width="250"></td>
		</tr>
		<tr>
			<td width="45px">6.</td>
			<td width="200px">Panjang Kapal (LOA)</td>
			<td width="5px">:</td>
			<td width="255px" class="garis-hitam"></td>
			<td width="250"></td>
		</tr>
		<tr>
			<td width="45px">7.</td>
			<td width="200px">Tonnase Kapal</td>
			<td width="5px">:</td>
			<td width="255px" class="garis-hitam"></td>
			<td width="250"></td>
		</tr>
		<tr>
			<td width="45px">8.</td>
			<td width="200px">Tanggal Kedatangan</td>
			<td width="5px">:</td>
			<td width="255px" class="garis-hitam"></td>
			<td width="250"></td>
		</tr>
		<tr>
			<td width="45px">9.</td>
			<td width="200px">Tanggal Berangkat</td>
			<td width="5px">:</td>
			<td width="255px" class="garis-hitam"></td>
			<td width="250"></td>
		</tr>
		<tr>
			<td width="45px">10.</td>
			<td width="200px">Jenis Pelayanan</td>
			<td width="5px">:</td>
			<td width="255px" class="garis-hitam"></td>
			<td width="250"></td>
		</tr>
	</table>

	<table class="table-pdf">
		<tr>
			<td width="50">NO</td>
			<td width="250">JENIS JASA</td>
			<td width="450">PERHITUNGAN</td>
			<td width="200">JUMLAH</td>
		</tr>
		<tr>
			<td width="50">1.</td>
			<td width="250">Labuh</td>
			<td width="450"></td>
			<td width="200"></td>
		</tr>
		<tr>
			<td width="50">2.</td>
			<td width="250">Tambat</td>
			<td width="450"></td>
			<td width="200"></td>
		</tr>
		<tr>
			<td width="50">3.</td>
			<td width="250">Dermaga</td>
			<td width="450"></td>
			<td width="200"></td>
		</tr>
		<tr>
			<td width="50">4.</td>
			<td width="250">Penumpukan</td>
			<td width="450"></td>
			<td width="200"></td>
		</tr>
	</table>

	<table class="table-pdf">
		<tr>
			<td width="800">TOTAL JUMLAH</td>
			<td width="215"></td>
		</tr>
	</table>

	<table class="table2">
		<tr>
			<td width="250">TERBILANG</td>
			<td width="5">:</td>
			<td width="650">ENAM PULUH TUJUH RIBU DUA RATUS ENAM RUPIAH</td>
		</tr>
	</table>

	<p class="line" style="font-size: 10px">CATATAN :</p><br>

	<p style="font-size: 10px">Tempat Pembayaran dan Nomer Rekening Kas Daerah Provinsi Kepulauan Riau</p>

	<table class="table">
		<tr>
			<td width="40px">1.</td>
			<td width="120px">Bank Riau</td>
			<td width="5px">:</td>
			<td width="700px">103-01-00310</td>
		</tr>
		<tr>
			<td width="40px">2.</td>
			<td width="120px">Bank Bukopin</td>
			<td width="5px">:</td>
			<td width="700px">10.013.702.40</td>
		</tr>
	</table>

	<table class="table">
		<tr>
			<td width="300">Ditetapkan oleh</td>
			<td width="400"></td>
			<td width="300">Tanjungpinang, 11 Oktober 2018 Ditetapkan oleh Dihitung oleh</td>
		</tr>
		<tr>
			<td width="300">ttd</td>
			<td width="400"></td>
			<td width="300">ttd</td>
		</tr>
		<tr>
			<td width="300" class="garis-hitam">MARTONO, ST</td>
			<td width="400"></td>
			<td width="300" class="garis-hitam">ANDRY BERNARD. S</td>
		</tr>
		<tr>
			<td width="300">NIP. 19800914 200803 1 001</td>
			<td width="400"></td>
			<td width="300">NIP. ANDRY BERNARD. S</td>
		</tr>
	</table>

	<table class="table">
		<tr>
			<td width="250"><p style="font-size: 15px">Menyutuji</p></td>
			<td width="650"></td>
		</tr>
		<tr>
			<td width="250">PT. SANDY OCTASURI AGUNG JAYA</td>
			<td width="650"></td>
		</tr>
		<tr>
			<td width="250">ttd</td>
			<td width="650"></td>
		</tr>
		<tr>
			<td width="250" class="garis-hitam">M. AZHAR</td>
			<td width="650"></td>
		</tr>
	</table>

	<p style="font-size: 10px">*) Coret yang tidak perlu</p>
	
</div>