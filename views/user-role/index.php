<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserRoleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kategori User';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-role-index box box-primary">

    <div class="box-header">
        <h3 class="box-title">Daftar Kategori User.</h3>
    </div>

    <div class="box-body table-responsive">

        <?php /*<h1><?= Html::encode($this->title) ?></h1>*/ ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a('<i class="fa fa-plus"> Tambah Kategori User</i>', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                'nama',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        
    </div>

</div>
