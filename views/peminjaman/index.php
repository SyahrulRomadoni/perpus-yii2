<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Buku;
use app\models\Anggota;
use kartik\date\DatePicker;
use kartik\number\NumberControl;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PeminjamanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Peminjaman';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Index peminjaman admin -->
<?php if (Yii::$app->user->identity->id_user_role == 1): ?>

    <div class="peminjaman-index box box-primary">

        <div class="box-header">
            <h3 class="box-title">Daftar Peminjaman Buku.</h3>
        </div>

        <div class="box-body table-responsive">

            <?php /*<h1><?= Html::encode($this->title) ?></h1>*/ ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a('<i class="fa fa-plus"> Tambah Peminjaman</i>', ['create'], ['class' => 'btn btn-success']) ?>
                <?= Html::a('<i class="fa fa-print"> Export Daftar Peminjaman Buku ke Word</i>', ['daftar-peminjaman-buku-word'], ['class' => 'btn btn-info btn-flat']) ?>
            </p>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'id',
                    //'id_buku',
                    [  
                        'attribute' => 'id_buku',
                        'value' => function($data)
                        {
                            // Cara 1 Pemanggil id_*** menjadi nama.
                            //return $data->getPenulis();

                            // Cara 2 Pemanggil id_*** menjadi nama.
                            return $data->buku->nama;
                        },
                        'filter'=>Buku::getList(),
                    ],
                    //'id_anggota',
                    [  
                        'attribute' => 'id_anggota',
                        'label'=>'Nama<br>Anggota',
                        'encodeLabel'=>false,
                        'value' => function($data)
                        {
                            // Cara 1 Pemanggil id_*** menjadi nama.
                            //return $data->getPenulis();

                            // Cara 2 Pemanggil id_*** menjadi nama.
                            return $data->anggota->nama;
                        },
                        'filter'=>Anggota::getList(),
                    ],
                    //'tanggal_pinjam',
                    [
                        'attribute'=>'tanggal_pinjam',
                        'label'=>'Tanggal<br>Pinjam',
                        'encodeLabel'=>false,
                        'filter'=>DatePicker::widget([
                            'model'=>$searchModel,
                            'attribute'=>'tanggal_pinjam',
                            'pluginOptions'=>[
                                'format' => 'yyyy-mm-dd'
                            ]
                        ])
                    ],
                    // [
                    //     'attribute' => 'tanggal_pinjam',
                    //     'format'=> ['DateTime','php: Y / F / d - D'],
                    // ],
                    //'tanggal_kembali',
                    [
                        'attribute'=>'tanggal_kembali',
                        'label'=>'Tanggal<br>Kembali',
                        'encodeLabel'=>false,
                        'filter'=>DatePicker::widget([
                            'model'=>$searchModel,
                            'attribute'=>'tanggal_kembali',
                            'pluginOptions'=>[
                                'format' => 'yyyy-mm-dd'
                            ]
                        ])
                    ],
                    // [
                    //     'attribute' => 'tanggal_kembali',
                    //     'format'=> ['DateTime','php: Y / F / d - D'],
                    // ],
                    //'status_buku',
                    [
                        'attribute' => 'status_buku',
                        'label'=>'Status<br>Buku',
                        'encodeLabel'=>false,
                        'value' => function ($model) {
                            if ($model->status_buku == 0) {
                                return "Belum Di Kembalikan";
                            };
                            if ($model->status_buku == 1) {
                                return "Masih Di Pinjam";
                            };
                            if ($model->status_buku == 2) {
                                return "Sudah Di Kembalikan";
                            };
                        },
                        'filter'=>[
                            0 => 'Belum Di Kembalikan',
                            1 => 'Masih Di Pinjam',
                            2 => 'Sudah Di Kembalikan',
                        ],
                    ],
                    // Telat
                    [
                        'attribute' => 'Telat',
                        'value' => function ($model) {
                            if ($model->status_buku == 2) {
                                return "- Hari";
                            };
                            if ($model->tanggal_kembali <= date('Y-m-d')) {
                                return $model->getSelisih() . " Hari";
                            } else {
                                return "0 Hari";
                            };
                        },
                        'headerOptions'=>['style'=>'text-align:center; width: 80px'],
                        'contentOptions'=>['style'=>'text-align:center'],
                    ],
                    //'tanggal_pengembalian_buku',
                    [
                        'attribute'=>'tanggal_pengembalian_buku',
                        'label'=>'Tanggal<br>Pengembalian<br>Buku',
                        'encodeLabel'=>false,
                        'filter'=>DatePicker::widget([
                            'model'=>$searchModel,
                            'attribute'=>'tanggal_pengembalian_buku',
                            'pluginOptions'=>[
                                'format' => 'yyyy-mm-dd'
                            ]
                        ]),
                    ],
                    //'harga',
                    [
                        'attribute'=>'harga',
                        'filter'=>NumberControl::widget([
                            'model'=>$searchModel,
                            'attribute'=>'harga',
                        ]),
                        'value' =>function($data) {
                            return "Rp. " . number_format($data->harga,2);
                        },
                        'headerOptions'=>['style'=>'text-align:center; width: 80px'],
                        'contentOptions'=>['style'=>'text-align:center'],
                    ],


                    //['class' => 'yii\grid\ActionColumn'],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {update} {kembalikan} {delete}',
                        'buttons' => [
                            'kembalikan' => function($url, $model, $key) {
                                return Html::a('<i class="fa fa-check-square-o"></i>', ['kembalikan-buku', 'id' => $model->id], ['data' => ['confirm' => 'Apa anda yakin ingin mengembalikan Buku ini?'],]);
                            }
                        ]
                    ],
                ],
            ]); ?>

        <div class="box-body">

    </div>

<?php endif ?>

<!-- Index peminjaman anggota -->
<?php if (Yii::$app->user->identity->id_user_role == 2): ?>

    <div class="peminjaman-index box box-primary">

        <div class="box-header">
            <h3 class="box-title">Daftar Peminjaman Buku.</h3>
        </div>

        <div class="box-body table-responsive">

            <?php /*<h1><?= Html::encode($this->title) ?></h1>*/ ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?php /*
            <p>
                <?= Html::a('<i class="fa fa-plus"> Tambah Peminjaman</i>', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
            */ ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'id',
                    //'id_buku',
                    [  
                        'attribute' => 'id_buku',
                        'value' => function($data)
                        {
                            // Cara 1 Pemanggil id_*** menjadi nama.
                            //return $data->getPenulis();

                            // Cara 2 Pemanggil id_*** menjadi nama.
                            return $data->buku->nama;
                        },
                        'filter'=>Buku::getList(),
                    ],
                    //'id_anggota',
                    /*[  
                        'attribute' => 'id_anggota',
                        'value' => function($data)
                        {
                            // Cara 1 Pemanggil id_*** menjadi nama.
                            //return $data->getPenulis();

                            // Cara 2 Pemanggil id_*** menjadi nama.
                            return $data->anggota->nama;
                        }
                    ],*/
                    //'tanggal_pinjam',
                    [
                        'attribute'=>'tanggal_pinjam',
                        'label'=>'Tanggal<br>Pinjam',
                        'encodeLabel'=>false,
                        'filter'=>DatePicker::widget([
                            'model'=>$searchModel,
                            'attribute'=>'tanggal_pinjam',
                            'pluginOptions'=>[
                                'format' => 'yyyy-mm-dd'
                            ]
                        ])
                    ],
                    // [
                    //     'attribute' => 'tanggal_pinjam',
                    //     'format'=> ['DateTime','php: Y / F / d - D'],
                    // ],
                    //'tanggal_kembali',
                    [
                        'attribute'=>'tanggal_kembali',
                        'label'=>'Tanggal<br>Kembali',
                        'encodeLabel'=>false,
                        'filter'=>DatePicker::widget([
                            'model'=>$searchModel,
                            'attribute'=>'tanggal_kembali',
                            'pluginOptions'=>[
                                'format' => 'yyyy-mm-dd'
                            ]
                        ])
                    ],
                    // [
                    //     'attribute' => 'tanggal_kembali',
                    //     'format'=> ['DateTime','php: Y / F / d - D'],
                    // ],
                    [
                        'attribute' => 'status_buku',
                        'label'=>'Status<br>Buku',
                        'encodeLabel'=>false,
                        'value' => function ($model) {
                            if ($model->status_buku == 0) {
                                return "Belum Di Kembalikan";
                            };
                            if ($model->status_buku == 1) {
                                return "Masih Di Pinjam";
                            };
                            if ($model->status_buku == 2) {
                                return "Sudah Di Kembalikan";
                            };
                        },
                        'filter'=>[
                            0 => 'Belum Di Kembalikan',
                            1 => 'Masih Di Pinjam',
                            2 => 'Sudah Di Kembalikan',
                        ],
                    ],
                    // Telat
                    [
                        'attribute' => 'Telat',
                        'value' => function ($model) {
                            if ($model->status_buku == 2) {
                                return "- Hari";
                            };
                            if ($model->tanggal_kembali <= date('Y-m-d')) {
                                return $model->getSelisih() . " Hari";
                            } else {
                                return "0 Hari";
                            };
                        },
                        'headerOptions'=>['style'=>'text-align:center; width: 80px'],
                        'contentOptions'=>['style'=>'text-align:center'],
                    ],
                    //'tanggal_pengembalian_buku',
                    [
                        'attribute'=>'tanggal_pengembalian_buku',
                        'label'=>'Tanggal<br>Pengembalian<br>Buku',
                        'encodeLabel'=>false,
                        'filter'=>DatePicker::widget([
                            'model'=>$searchModel,
                            'attribute'=>'tanggal_pengembalian_buku',
                            'pluginOptions'=>[
                                'format' => 'yyyy-mm-dd'
                            ]
                        ])
                    ],
                    //'harga',
                    [
                        'attribute'=>'harga',
                        'filter'=>NumberControl::widget([
                            'model'=>$searchModel,
                            'attribute'=>'harga',
                        ]),
                        'value' =>function($data) {
                            return "Rp. " . number_format($data->harga,2);
                        },
                        'headerOptions'=>['style'=>'text-align:center; width: 80px'],
                        'contentOptions'=>['style'=>'text-align:center'],
                    ],

                    //['class' => 'yii\grid\ActionColumn'],
                    /* [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{kembalikan}',
                        'buttons' => [
                            'kembalikan' => function($url, $model, $key) {
                                return Html::a('<i class="fa fa-book"></i>', ['kembalikan-buku', 'id' => $model->id], ['data' => ['confirm' => 'Apa anda yakin ingin mengembalikan Buku ini?'],]);
                            }
                        ]
                    ], */
                ],
            ]); ?>

        </div>

    </div>
    
<?php endif ?>

<!-- Index peminjaman petugas -->
<?php if (Yii::$app->user->identity->id_user_role == 3): ?>

    <div class="peminjaman-index box box-primary">

        <div class="box-header">
            <h3 class="box-title">Daftar Peminjaman Buku.</h3>
        </div>

        <div class="box-body table-responsive">

            <?php /*<h1><?= Html::encode($this->title) ?></h1>*/ ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a('<i class="fa fa-plus"> Tambah Peminjaman</i>', ['create'], ['class' => 'btn btn-success']) ?>
                <?= Html::a('<i class="fa fa-print"> Export Daftar Peminjaman Buku ke Word</i>', ['daftar-peminjaman-buku-word'], ['class' => 'btn btn-info btn-flat']) ?>
            </p>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    //'id',
                    //'id_buku',
                    [  
                        'attribute' => 'id_buku',
                        'value' => function($data)
                        {
                            // Cara 1 Pemanggil id_*** menjadi nama.
                            //return $data->getPenulis();

                            // Cara 2 Pemanggil id_*** menjadi nama.
                            return $data->buku->nama;
                        },
                        'filter'=>Buku::getList(),
                    ],
                    //'id_anggota',
                    [  
                        'attribute' => 'id_anggota',
                        'label'=>'Nama<br>Anggota',
                        'encodeLabel'=>false,
                        'value' => function($data)
                        {
                            // Cara 1 Pemanggil id_*** menjadi nama.
                            //return $data->getPenulis();

                            // Cara 2 Pemanggil id_*** menjadi nama.
                            return $data->anggota->nama;
                        },
                        'filter'=>Anggota::getList(),
                    ],
                    //'tanggal_pinjam',
                    [
                        'attribute'=>'tanggal_pinjam',
                        'label'=>'Tanggal<br>Pinjam',
                        'encodeLabel'=>false,
                        'filter'=>DatePicker::widget([
                            'model'=>$searchModel,
                            'attribute'=>'tanggal_pinjam',
                            'pluginOptions'=>[
                                'format' => 'yyyy-mm-dd'
                            ]
                        ])
                    ],
                    // [
                    //     'attribute' => 'tanggal_pinjam',
                    //     'format'=> ['DateTime','php: Y / F / d - D'],
                    // ],
                    //'tanggal_kembali',
                    [
                        'attribute'=>'tanggal_kembali',
                        'label'=>'Tanggal<br>Kembali',
                        'encodeLabel'=>false,
                        'filter'=>DatePicker::widget([
                            'model'=>$searchModel,
                            'attribute'=>'tanggal_kembali',
                            'pluginOptions'=>[
                                'format' => 'yyyy-mm-dd'
                            ]
                        ])
                    ],
                    // [
                    //     'attribute' => 'tanggal_kembali',
                    //     'format'=> ['DateTime','php: Y / F / d - D'],
                    // ],
                    //'status_buku',
                    [
                        'attribute' => 'status_buku',
                        'label'=>'Status<br>Buku',
                        'encodeLabel'=>false,
                        'value' => function ($model) {
                            if ($model->status_buku == 0) {
                                return "Belum Di Kembalikan";
                            };
                            if ($model->status_buku == 1) {
                                return "Masih Di Pinjam";
                            };
                            if ($model->status_buku == 2) {
                                return "Sudah Di Kembalikan";
                            };
                        },
                        'filter'=>[
                            0 => 'Belum Di Kembalikan',
                            1 => 'Masih Di Pinjam',
                            2 => 'Sudah Di Kembalikan',
                        ],
                    ],
                    // Telat
                    [
                        'attribute' => 'Telat',
                        'value' => function ($model) {
                            if ($model->status_buku == 2) {
                                return "- Hari";
                            };
                            if ($model->tanggal_kembali <= date('Y-m-d')) {
                                return $model->getSelisih() . " Hari";
                            } else {
                                return "0 Hari";
                            };
                        },
                        'headerOptions'=>['style'=>'text-align:center; width: 80px'],
                        'contentOptions'=>['style'=>'text-align:center'],
                    ],
                    //'tanggal_pengembalian_buku',
                    [
                        'attribute'=>'tanggal_pengembalian_buku',
                        'label'=>'Tanggal<br>Pengembalian<br>Buku',
                        'encodeLabel'=>false,
                        'filter'=>DatePicker::widget([
                            'model'=>$searchModel,
                            'attribute'=>'tanggal_pengembalian_buku',
                            'pluginOptions'=>[
                                'format' => 'yyyy-mm-dd'
                            ]
                        ]),
                    ],
                    //'harga',
                    [
                        'attribute'=>'harga',
                        'filter'=>NumberControl::widget([
                            'model'=>$searchModel,
                            'attribute'=>'harga',
                        ]),
                        'value' =>function($data) {
                            return "Rp. " . number_format($data->harga,2);
                        },
                        'headerOptions'=>['style'=>'text-align:center; width: 80px'],
                        'contentOptions'=>['style'=>'text-align:center'],
                    ],


                    //['class' => 'yii\grid\ActionColumn'],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {update} {kembalikan} {delete}',
                        'buttons' => [
                            'kembalikan' => function($url, $model, $key) {
                                return Html::a('<i class="fa fa-check-square-o"></i>', ['kembalikan-buku', 'id' => $model->id], ['data' => ['confirm' => 'Apa anda yakin ingin mengembalikan Buku ini?'],]);
                            }
                        ]
                    ],
                ],
            ]); ?>

        </div>

    </div>
    
<?php endif ?>