<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BukuSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="buku-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'action' => ['site/dashboard'],
    ]); ?>

    <?= $form->field($model,'pencarian_buku')->label(''); ?>

    <?php /*

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'tahun_terbit') ?>

    <?= $form->field($model, 'id_penulis') ?>

    <?= $form->field($model, 'id_penerbit') ?>

    <?= $form->field($model, 'id_kategori') ?>

    <?= $form->field($model, 'sinopsis') ?>

    <?= $form->field($model, 'sampul') ?>

    <?= $form->field($model, 'berkas') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    */ ?>

    <?php ActiveForm::end(); ?>

</div>
