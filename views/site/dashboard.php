<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use miloschuman\highcharts\Highcharts;
use app\models\Buku;
use app\models\BukuSearch;
use app\models\Kategori;
use app\models\Penerbit;
use app\models\Penulis;
use app\models\Anggota;
use app\models\Petugas;
use app\models\User;
use app\models\Peminjaman;
use yii\widgets\LinkPager;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;

$this->title = 'Perpustakaan Dashboard';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Dashboard Admin -->
<?php if (Yii::$app->user->identity->id_user_role == 1): ?>

    <div class="row">

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <p>Jumlah Anggota</p>

                    <h3><?= Yii::$app->formatter->asInteger(Anggota::getCount()); ?></h3>
                </div>
                <div class="icon">
                    <i class="fa fa-user"></i>
                </div>
                <a href="<?=Url::to(['anggota/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <p>Jumlah Petugas</p>

                    <h3><?= Yii::$app->formatter->asInteger(Petugas::getCount()); ?></h3>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
                <a href="<?=Url::to(['petugas/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-gray">
                <div class="inner">
                    <p>Jumlah User</p>

                    <h3><?= Yii::$app->formatter->asInteger(User::getCount()); ?></h3>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
                <a href="<?=Url::to(['user/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-maroon">
                <div class="inner">
                    <p>Jumlah Penulis</p>

                    <h3><?= Yii::$app->formatter->asInteger(Penulis::getCount()); ?></h3>
                </div>
                <div class="icon">
                    <i class="fa fa-user"></i>
                </div>
                <a href="<?=Url::to(['penulis/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <p>Jumlah Buku</p>

                    <h3><?= Yii::$app->formatter->asInteger(Buku::getCount()); ?></h3>
                </div>
                <div class="icon">
                    <i class="fa fa-book"></i>
                </div>
                <a href="<?=Url::to(['buku/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <p>Jumlah Kategori</p>

                    <h3><?= Yii::$app->formatter->asInteger(Kategori::getCount()); ?></h3>
                </div>
                <div class="icon">
                    <i class="fa fa-cubes"></i>
                </div>
                <a href="<?=Url::to(['kategori/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <p>Jumlah Penerbit</p>

                    <h3><?= Yii::$app->formatter->asInteger(Penerbit::getCount()); ?></h3>
                </div>
                <div class="icon">
                    <i class="fa fa-building"></i>
                </div>
                <a href="<?=Url::to(['penerbit/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-orange">
                <div class="inner">
                    <p>Jumlah Peminjaman</p>

                    <h3><?= Yii::$app->formatter->asInteger(Peminjaman::getCount()); ?></h3>
                </div>
                <div class="icon">
                    <i class="fa fa-building"></i>
                </div>
                <a href="<?=Url::to(['peminjaman/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-sm-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Grafik Kategori Berdasarkan Buku</h3>
                </div>
                <div class="box-body">
                    <?=Highcharts::widget([
                    'options' => [
                        'credits' => false,
                        'title' => ['text' => 'KATEGORI'],
                        'exporting' => ['enabled' => true],
                        'plotOptions' => [
                            'pie' => [
                                'cursor' => 'pointer',
                            ],
                        ],
                        'series' => [
                            [
                                'type' => 'pie',
                                'name' => 'Kategori',
                                'data' => Kategori::getGrafikList(),
                            ],
                        ],
                    ],
                ]);?>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Grafik Penerbit Berdasarkan Buku</h3>
                </div>
                <div class="box-body">
                    <?=Highcharts::widget([
                    'options' => [
                        'credits' => false,
                        'title' => ['text' => 'PENERBIT'],
                        'exporting' => ['enabled' => true],
                        'plotOptions' => [
                            'pie' => [
                                'cursor' => 'pointer',
                            ],
                        ],
                        'series' => [
                            [
                                'type' => 'pie',
                                'name' => 'Penerbit',
                                'data' => Penerbit::getGrafikList(),
                            ],
                        ],
                    ],
                ]);?>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Grafik Penulis Berdasarkan Buku</h3>
                </div>
                <div class="box-body">
                    <?=Highcharts::widget([
                    'options' => [
                        'credits' => false,
                        'title' => ['text' => 'PENULIS'],
                        'exporting' => ['enabled' => true],
                        'plotOptions' => [
                            'pie' => [
                                'cursor' => 'pointer',
                            ],
                        ],
                        'series' => [
                            [
                                'type' => 'pie',
                                'name' => 'Penulis',
                                'data' => Penulis::getGrafikList(),
                            ],
                        ],
                    ],
                ]);?>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Grafik Peminjaman Berdasarkan Buku</h3>
                </div>
                <div class="box-body">
                    <?= \miloschuman\highcharts\Highcharts::widget([
                    'options' => [
                        'credits' => true,
                        'title' => ['text' => 'PEMINJAMAN'],
                        'exporting' => ['enabled' => true],
                        'xAxis' => [
                            'categories' => \app\components\Helper::getListBulanGrafik(),
                        ],
                        'series' => [
                            [
                                'type' => 'column',
                                'colorByPoint' => true,
                                'name' => 'Peminjaman',
                                'data' => \app\models\Peminjaman::getCountGrafik(),
                                'showInLegend' => false
                            ],
                        ],
                    ]
                ]) ?>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Daftar Peminjaman Yang Baru Meminjam Buku.
                    </h3>
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-bordered table-stripped">
                        <thead class="">
                            <tr>
                                <th width="55px" class="text-center" rowspan="2">NO</th>
                                <th class="text-center" rowspan="2">Nama Buku</th>
                                <th class="text-center" rowspan="2">Nama Peminjam</th>
                                <th class="text-center" colspan="3">Tanggal</th>
                                <th width="85px" class="text-center" rowspan="2">Status</th>
                            </tr>
                            <tr>
                                <th width="150px" class="text-center">Pinjam</th>
                                <th width="150px" class="text-center">Batas Kembali</th>
                                <th width="150px" class="text-center">Tanggal Kembalikan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1?>
                            <?php foreach (Peminjaman::find()->andWhere(['status_buku' => 1])->orderBy(['tanggal_pinjam' =>  SORT_DESC])->limit(10)->all() as $peminjam): ?>
                            <tr>
                                <td class="text-center"><?= $i++ ?></td>
                                <td class="text-center"><?= $peminjam->buku->nama ?></td>
                                <td class="text-center"><?= $peminjam->anggota->nama ?></td>
                                <td class="text-center"><?= $peminjam->tanggal_pinjam ?></td>
                                <td class="text-center"><?= $peminjam->tanggal_kembali ?></td>
                                <td class="text-center"><?= $peminjam->tanggal_pengembalian_buku ?></td>
                                <td class="text-center"><?= $peminjam->getStatusPeminjaman() ?></td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Daftar Peminjaman Yang Telat Mengembalikan Buku.
                    </h3>
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-bordered table-stripped">
                        <thead class="">
                            <tr>
                                <th width="55px" class="text-center" rowspan="2">NO</th>
                                <th class="text-center" rowspan="2">Nama Buku</th>
                                <th class="text-center" rowspan="2">Nama Peminjam</th>
                                <th class="text-center" colspan="3">Tanggal</th>
                                <th width="85px" class="text-center" rowspan="2">Telat</th>
                                <th width="85px" class="text-center" rowspan="2">Status</th>
                                <th width="55px" class="text-center" rowspan="2">Cek Buku</th>
                            </tr>
                            <tr>
                                <th width="150px" class="text-center">Pinjam</th>
                                <th width="150px" class="text-center">Batas Kembali</th>
                                <th width="150px" class="text-center">Pengembalian</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1?>
                            <?php foreach (Peminjaman::find()->andWhere(['status_buku' => 0])->orderBy(['tanggal_pinjam' =>  SORT_DESC])->limit(10)->all() as $peminjam): ?>
                            <tr>
                                <td class="text-center"><?= $i++ ?></td>
                                <td class="text-center"><?= $peminjam->buku->nama ?></td>
                                <td class="text-center"><?= $peminjam->anggota->nama ?></td>
                                <td class="text-center"><?= $peminjam->tanggal_pinjam ?></td>
                                <td class="text-center"><?= $peminjam->tanggal_kembali ?></td>
                                <td class="text-center"><?= $peminjam->tanggal_pengembalian_buku ?></td>
                                <td class="text-center"><?= $peminjam->getSelisih() ?> Hari</td>
                                <td class="text-center"><?= $peminjam->getStatusPeminjaman() ?></td>
                                <td class="text-center"><?= Html::a('<i class="fa fa-check-square-o"></i>', ['peminjaman/kembalikan-buku', 'id' => $peminjam->id], ['data' => ['confirm' => 'Apa anda yakin ingin menyutujui Buku ini?'],]); ?>
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Daftar Peminjaman Yang Telah Mengembalikan Buku.
                    </h3>
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-bordered table-stripped">
                        <thead class="">
                            <tr>
                                <th width="55px" class="text-center" rowspan="2">NO</th>
                                <th class="text-center" rowspan="2">Nama Buku</th>
                                <th class="text-center" rowspan="2">Nama Peminjam</th>
                                <th class="text-center" colspan="3">Tanggal</th>
                                <th width="85px" class="text-center" rowspan="2">Status</th>
                            </tr>
                            <tr>
                                <th width="150px" class="text-center">Pinjam</th>
                                <th width="150px" class="text-center">Batas Kembali</th>
                                <th width="150px" class="text-center">Tanggal Kembalikan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1?>
                            <?php foreach (Peminjaman::find()->andWhere(['status_buku' => 2])->orderBy(['tanggal_pinjam' =>  SORT_DESC])->limit(10)->all() as $peminjam): ?>
                            <tr>
                                <td class="text-center"><?= $i++ ?></td>
                                <td class="text-center"><?= $peminjam->buku->nama ?></td>
                                <td class="text-center"><?= $peminjam->anggota->nama ?></td>
                                <td class="text-center"><?= $peminjam->tanggal_pinjam ?></td>
                                <td class="text-center"><?= $peminjam->tanggal_kembali ?></td>
                                <td class="text-center"><?= $peminjam->tanggal_pengembalian_buku ?></td>
                                <td class="text-center"><?= $peminjam->getStatusPeminjaman() ?></td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
<?php endif ?>

<!-- Dashboard Anggota -->
<?php if (Yii::$app->user->identity->id_user_role == 2): ?>
<?php $this->title = 'Perpustakaan'; ?>

<div class="row">
    <div class="col-sm-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Pencarian Buku.</h3>
            </div>
            <div class="box-body">
                <?= $this->render('_search', ['model' => $searchModel]); ?>
            </div>
        </div>
    </div>
</div>

<div class="row">

    <?php foreach ($provider->getModels() as $buku) {?> 
        <!-- Kolom box mulai -->
        <div class="col-md-4" style="height: 550px;">

            <!-- Box mulai -->
            <div class="box box-widget">

                <div class="box-header with-border">
                    <div class="user-block">
                        <img class="img-circle" src="<?= Yii::getAlias('@web').'/images/P2.jpg'; ?>" alt="User Image">
                        <span class="username"><?= Html::a($buku->nama, ['buku/view', 'id' => $buku->id]); ?></span>
                        <span class="description"> Di Terbitkan : Tahun <?= $buku->tahun_terbit; ?></span>
                    </div>
                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read"><i class="fa fa-circle-o"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <center>
                        <img style="height: 300px" class="img-responsive pad" src="<?= Yii::$app->request->baseUrl.'/upload/'.$buku['sampul']; ?>" alt="Photo">
                    </center>
                    <p>Sinopsis : <?= substr($buku->sinopsis,0,120);?> ...</p>
                    <?= Html::a("<i class='fa fa-eye'> Detail Buku</i>",["buku/view","id"=>$buku->id],['class' => 'btn btn-default']) ?>
                    <?= Html::a('<i class="fa fa-file"> Pinjam Buku</i>', ['peminjaman/create', 'id_buku' => $buku->id], ['class' => 'btn btn-primary',
                        'data' => [
                            'confirm' => 'Apa anda yakin ingin meminjam buku ini?',
                        ],
                    ]) ?>
                    <!-- <span class="pull-right text-muted">127 Peminjam - 3 Komentar</span> -->
                </div>

            </div>
            <!-- Box selesai -->

        </div>
        <!-- Kolom box selesai -->  
    <?php
        }
    ?>
</div>

<!-- Pagingation -->
<div class="row">
    <center>
        <?= LinkPager::widget([
            'pagination' => $provider->pagination,
        ])?>
    </center>
</div>

<?php endif ?>

<!-- Dashboard Petugas -->
<?php if (Yii::$app->user->identity->id_user_role == 3): ?>

    <div class="row">

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <p>Jumlah Anggota</p>

                    <h3><?= Yii::$app->formatter->asInteger(Anggota::getCount()); ?></h3>
                </div>
                <div class="icon">
                    <i class="fa fa-user"></i>
                </div>
                <a href="<?=Url::to(['anggota/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <p>Jumlah Petugas</p>

                    <h3><?= Yii::$app->formatter->asInteger(Petugas::getCount()); ?></h3>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
                <a href="<?=Url::to(['petugas/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-gray">
                <div class="inner">
                    <p>Jumlah User</p>

                    <h3><?= Yii::$app->formatter->asInteger(User::getCount()); ?></h3>
                </div>
                <div class="icon">
                    <i class="fa fa-users"></i>
                </div>
                <a href="<?=Url::to(['user/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-maroon">
                <div class="inner">
                    <p>Jumlah Penulis</p>

                    <h3><?= Yii::$app->formatter->asInteger(Penulis::getCount()); ?></h3>
                </div>
                <div class="icon">
                    <i class="fa fa-user"></i>
                </div>
                <a href="<?=Url::to(['penulis/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <p>Jumlah Buku</p>

                    <h3><?= Yii::$app->formatter->asInteger(Buku::getCount()); ?></h3>
                </div>
                <div class="icon">
                    <i class="fa fa-book"></i>
                </div>
                <a href="<?=Url::to(['buku/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <p>Jumlah Kategori</p>

                    <h3><?= Yii::$app->formatter->asInteger(Kategori::getCount()); ?></h3>
                </div>
                <div class="icon">
                    <i class="fa fa-cubes"></i>
                </div>
                <a href="<?=Url::to(['kategori/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <p>Jumlah Penerbit</p>

                    <h3><?= Yii::$app->formatter->asInteger(Penerbit::getCount()); ?></h3>
                </div>
                <div class="icon">
                    <i class="fa fa-building"></i>
                </div>
                <a href="<?=Url::to(['penerbit/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-orange">
                <div class="inner">
                    <p>Jumlah Peminjaman</p>

                    <h3><?= Yii::$app->formatter->asInteger(Peminjaman::getCount()); ?></h3>
                </div>
                <div class="icon">
                    <i class="fa fa-building"></i>
                </div>
                <a href="<?=Url::to(['peminjaman/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

    </div>

    <div class="row">

        <div class="col-sm-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Grafik Kategori Berdasarkan Buku</h3>
                </div>
                <div class="box-body">
                    <?=Highcharts::widget([
                    'options' => [
                        'credits' => false,
                        'title' => ['text' => 'KATEGORI'],
                        'exporting' => ['enabled' => true],
                        'plotOptions' => [
                            'pie' => [
                                'cursor' => 'pointer',
                            ],
                        ],
                        'series' => [
                            [
                                'type' => 'pie',
                                'name' => 'Kategori',
                                'data' => Kategori::getGrafikList(),
                            ],
                        ],
                    ],
                ]);?>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Grafik Penerbit Berdasarkan Buku</h3>
                </div>
                <div class="box-body">
                    <?=Highcharts::widget([
                    'options' => [
                        'credits' => false,
                        'title' => ['text' => 'PENERBIT'],
                        'exporting' => ['enabled' => true],
                        'plotOptions' => [
                            'pie' => [
                                'cursor' => 'pointer',
                            ],
                        ],
                        'series' => [
                            [
                                'type' => 'pie',
                                'name' => 'Penerbit',
                                'data' => Penerbit::getGrafikList(),
                            ],
                        ],
                    ],
                ]);?>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Grafik Penulis Berdasarkan Buku</h3>
                </div>
                <div class="box-body">
                    <?=Highcharts::widget([
                    'options' => [
                        'credits' => false,
                        'title' => ['text' => 'PENULIS'],
                        'exporting' => ['enabled' => true],
                        'plotOptions' => [
                            'pie' => [
                                'cursor' => 'pointer',
                            ],
                        ],
                        'series' => [
                            [
                                'type' => 'pie',
                                'name' => 'Penulis',
                                'data' => Penulis::getGrafikList(),
                            ],
                        ],
                    ],
                ]);?>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Grafik Peminjaman Berdasarkan Buku</h3>
                </div>
                <div class="box-body">
                    <?= \miloschuman\highcharts\Highcharts::widget([
                    'options' => [
                        'credits' => true,
                        'title' => ['text' => 'PEMINJAMAN'],
                        'exporting' => ['enabled' => true],
                        'xAxis' => [
                            'categories' => \app\components\Helper::getListBulanGrafik(),
                        ],
                        'series' => [
                            [
                                'type' => 'column',
                                'colorByPoint' => true,
                                'name' => 'Peminjaman',
                                'data' => \app\models\Peminjaman::getCountGrafik(),
                                'showInLegend' => false
                            ],
                        ],
                    ]
                ]) ?>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Daftar Peminjaman Yang Baru Meminjam Buku.
                    </h3>
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-bordered table-stripped">
                        <thead class="">
                            <tr>
                                <th width="55px" class="text-center" rowspan="2">NO</th>
                                <th class="text-center" rowspan="2">Nama Buku</th>
                                <th class="text-center" rowspan="2">Nama Peminjam</th>
                                <th class="text-center" colspan="3">Tanggal</th>
                                <th width="85px" class="text-center" rowspan="2">Status</th>
                            </tr>
                            <tr>
                                <th width="150px" class="text-center">Pinjam</th>
                                <th width="150px" class="text-center">Batas Kembali</th>
                                <th width="150px" class="text-center">Tanggal Kembalikan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1?>
                            <?php foreach (Peminjaman::find()->andWhere(['status_buku' => 1])->orderBy(['tanggal_pinjam' =>  SORT_DESC])->limit(10)->all() as $peminjam): ?>
                            <tr>
                                <td class="text-center"><?= $i++ ?></td>
                                <td class="text-center"><?= $peminjam->buku->nama ?></td>
                                <td class="text-center"><?= $peminjam->anggota->nama ?></td>
                                <td class="text-center"><?= $peminjam->tanggal_pinjam ?></td>
                                <td class="text-center"><?= $peminjam->tanggal_kembali ?></td>
                                <td class="text-center"><?= $peminjam->tanggal_pengembalian_buku ?></td>
                                <td class="text-center"><?= $peminjam->getStatusPeminjaman() ?></td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Daftar Peminjaman Yang Telat Mengembalikan Buku.
                    </h3>
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-bordered table-stripped">
                        <thead class="">
                            <tr>
                                <th width="55px" class="text-center" rowspan="2">NO</th>
                                <th class="text-center" rowspan="2">Nama Buku</th>
                                <th class="text-center" rowspan="2">Nama Peminjam</th>
                                <th class="text-center" colspan="3">Tanggal</th>
                                <th width="85px" class="text-center" rowspan="2">Telat</th>
                                <th width="85px" class="text-center" rowspan="2">Status</th>
                                <th width="55px" class="text-center" rowspan="2">Cek Buku</th>
                            </tr>
                            <tr>
                                <th width="150px" class="text-center">Pinjam</th>
                                <th width="150px" class="text-center">Batas Kembali</th>
                                <th width="150px" class="text-center">Pengembalian</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1?>
                            <?php foreach (Peminjaman::find()->andWhere(['status_buku' => 0])->orderBy(['tanggal_pinjam' =>  SORT_DESC])->limit(10)->all() as $peminjam): ?>
                            <tr>
                                <td class="text-center"><?= $i++ ?></td>
                                <td class="text-center"><?= $peminjam->buku->nama ?></td>
                                <td class="text-center"><?= $peminjam->anggota->nama ?></td>
                                <td class="text-center"><?= $peminjam->tanggal_pinjam ?></td>
                                <td class="text-center"><?= $peminjam->tanggal_kembali ?></td>
                                <td class="text-center"><?= $peminjam->tanggal_pengembalian_buku ?></td>
                                <td class="text-center"><?= $peminjam->getSelisih() ?> Hari</td>
                                <td class="text-center"><?= $peminjam->getStatusPeminjaman() ?></td>
                                <td class="text-center"><?= Html::a('<i class="fa fa-check-square-o"></i>', ['peminjaman/kembalikan-buku', 'id' => $peminjam->id], ['data' => ['confirm' => 'Apa anda yakin ingin menyutujui Buku ini?'],]); ?>
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Daftar Peminjaman Yang Telah Mengembalikan Buku.
                    </h3>
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-bordered table-stripped">
                        <thead class="">
                            <tr>
                                <th width="55px" class="text-center" rowspan="2">NO</th>
                                <th class="text-center" rowspan="2">Nama Buku</th>
                                <th class="text-center" rowspan="2">Nama Peminjam</th>
                                <th class="text-center" colspan="3">Tanggal</th>
                                <th width="85px" class="text-center" rowspan="2">Status</th>
                            </tr>
                            <tr>
                                <th width="150px" class="text-center">Pinjam</th>
                                <th width="150px" class="text-center">Batas Kembali</th>
                                <th width="150px" class="text-center">Tanggal Kembalikan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 1?>
                            <?php foreach (Peminjaman::find()->andWhere(['status_buku' => 2])->orderBy(['tanggal_pinjam' =>  SORT_DESC])->limit(10)->all() as $peminjam): ?>
                            <tr>
                                <td class="text-center"><?= $i++ ?></td>
                                <td class="text-center"><?= $peminjam->buku->nama ?></td>
                                <td class="text-center"><?= $peminjam->anggota->nama ?></td>
                                <td class="text-center"><?= $peminjam->tanggal_pinjam ?></td>
                                <td class="text-center"><?= $peminjam->tanggal_kembali ?></td>
                                <td class="text-center"><?= $peminjam->tanggal_pengembalian_buku ?></td>
                                <td class="text-center"><?= $peminjam->getStatusPeminjaman() ?></td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
<?php endif ?>