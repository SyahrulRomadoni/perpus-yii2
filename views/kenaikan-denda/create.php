<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KenaikanDenda */

$this->title = 'Tambah Kenaikan Denda';
$this->params['breadcrumbs'][] = ['label' => 'Kenaikan Denda', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kenaikan-denda-create box box-primary">

	<div class="box-header">
        <h3 class="box-title">Tambah Kenaikan Denda.</h3>
    </div>

    <div class="box-body">

	    <?= $this->render('_form', [
	    'model' => $model,
	    ]) ?>

	</div>

</div>
