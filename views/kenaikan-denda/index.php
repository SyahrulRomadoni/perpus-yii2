<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\number\NumberControl;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KenaikanDendaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kenaikan Denda';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kenaikan-denda-index box box-primary">

    <div class="box-header">
        <h3 class="box-title">Kenaikan Denda.</h3>
    </div>

    <div class="box-body table-responsive">

        <p>
            <?php /*<?= Html::a('Tambah Kenaikan Denda', ['create'], ['class' => 'btn btn-success']) ?>*/ ?>
        </p>

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                // 'id',
                'hari',
                // 'harga',
                [
                    'attribute'=>'harga',
                    'filter'=>NumberControl::widget([
                        'model'=>$searchModel,
                        'attribute'=>'harga',
                    ]),
                    'value' =>function($data) {
                        return number_format($data->harga,2);
                    },
                    'headerOptions'=>['style'=>'text-align:center; width: 100px'],
                    'contentOptions'=>['style'=>'text-align:center'],
                ],

                //['class' => 'yii\grid\ActionColumn'],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update}',
                ],
            ],
        ]); ?>

    </div>

</div>
