<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\number\NumberControl;

/* @var $this yii\web\View */
/* @var $model app\models\KenaikanDenda */

$this->title = $model->harga;
$this->params['breadcrumbs'][] = ['label' => 'Kenaikan Denda', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kenaikan-denda-view box box-primary">

    <div class="box-header">
        <h3 class="box-title">Detail Kenaikan Denda : <?= $model->harga; ?>.</h3>
    </div>

    <div class="box-body">

        <p>
            <?= Html::a('<i class="fa fa-plus"> Tambah Kenaikan Denda</i>', ['create'], ['class' => 'btn btn-success']) ?>
            <?= Html::a('<i class="fa fa-pencil"> Edit</i>', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('<i class="fa fa-trash"> Hapus</i>', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                // 'id',
                'hari',
                // 'harga',
                [
                    'attribute'=>'harga',
                    'filter'=>NumberControl::widget([
                        'model'=>$model,
                        'attribute'=>'harga',
                    ]),
                    'value' =>function($data) {
                        return number_format($data->harga,2);
                    },
                    'headerOptions'=>['style'=>'text-align:center; width: 80px'],
                    'contentOptions'=>['style'=>'text-align:center'],
                ],
            ],
        ]) ?>

    </div>

</div>
