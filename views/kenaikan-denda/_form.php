<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\number\NumberControl;

/* @var $this yii\web\View */
/* @var $model app\models\KenaikanDenda */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kenaikan-denda-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'hari')->textInput() ?>

        <?php /*<?= $form->field($model, 'harga')->textInput() ?>*/ ?>
        <?= $form->field($model, 'harga')->widget(NumberControl::classname(), [
	        'name' => 'normal-decimal',
	    ]); ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
