<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KenaikanDenda */

$this->title = 'Update Kenaikan Denda: ' . $model->harga;
$this->params['breadcrumbs'][] = ['label' => 'Kenaikan Denda', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->harga, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="kenaikan-denda-update box box-primary">

	<div class="box-header">
        <h3 class="box-title">Edit Kenaikan Denda : <?= $model->harga; ?>.</h3>
    </div>

    <div class="box-body">

	    <?= $this->render('_form', [
	        'model' => $model,
	    ]) ?>

	</div>

</div>
