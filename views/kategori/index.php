<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KategoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kategori';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kategori-index box box-primary">

    <div class="box-header">
        <h3 class="box-title">Daftar Kategori.</h3>
    </div>

    <div class="box-body table-responsive">

        <?php /*<h1><?= Html::encode($this->title) ?></h1>*/ ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a('<i class="fa fa-plus"> Tambah Kategori</i>', ['create'], ['class' => 'btn btn-success']) ?>
            <?= Html::a('<i class="fa fa-print"> Export Daftar Kategori ke Word</i>', ['daftar-kategori-word'], ['class' => 'btn btn-info btn-flat']) ?>
        </p>

        <?php /*
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                'nama',
                [
                    'header' => 'Jumlah Buku',
                    'value' => function($model) {
                        return $model->getJumlahBuku();
                    }
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        */ ?>

        <?php
            $gridColumns = [
                ['class' => 'kartik\grid\SerialColumn',
                    //'width'=>'5%'
                ],
                [
                    'attribute' => 'nama',
                    'label' => 'Nama',
                    'vAlign' => 'middle',
                    'value' => function ($model) {
                        return $model->nama;
                    },
                ],
                [
                    'label' => 'Jumlah Buku',
                    'vAlign' => 'middle',
                    'value' => function($model) {
                        return $model->getJumlahBuku();
                    }
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ];

            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns,
                'containerOptions' => ['style' => 'overflow: auto'], 
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                'summary'=>'',
                'containerOptions' => ['style'=>'overflow: auto'], 
                'beforeHeader'=>[
                    [
                        'columns'=>[
                            ['content'=> $this->title, 'options'=>['colspan'=>12, 'class'=>'text-center warning']],
                        ],
                        'options'=>['class'=>'skip-export'] 
                    ]
                ],
                'exportConfig' => [
                      GridView::PDF => ['label' => 'Save as PDF'],
                      //GridView::TEXT => ['label' => 'Save as TEXT'],
                      GridView::EXCEL => ['label' => 'Save as EXCEL'],
                      //GridView::HTML => ['label' => 'Save as HTML'],
                      GridView::CSV => ['label' => 'Save as CSV'],
                  ],
                  
                'toolbar' =>  [
                    '{export}', 
                    //'{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                ],
                'pjax' => true,
                'bordered' => true,
                'striped' => true,
                'condensed' => false,
                'responsive' => false,
                'hover' => true,
                'floatHeader' => true,
                'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY
                ],

            ]);
        ?>

    </div>

</div>
