<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Petugas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="petugas-form">

    <?php $form = ActiveForm::begin([
    	// Membuat validasi misal nama atau apa sudah ada.
        //'id' => 'Kategori',
        'enableAjaxValidation' => true,
    ]); ?>

    <?php /*
    <?= $form->field($model, 'username')->textInput(['minlength' => 6, 'maxlength' => 255]) ?>

    <?= $form->field($model, 'password')->passwordInput(['minlength' => 6, 'maxlength' => 255]) ?>
    */ ?>

    <?= $form->field($model, 'nama')->textInput(['minlength' => 6, 'maxlength' => 255]) ?>

    <?= $form->field($model, 'alamat')->textInput(['minlength' => 6, 'maxlength' => 255]) ?>

    <?= $form->field($model, 'telepon')->textInput(['minlength' => 6, 'maxlength' => 13]) ?>

    <?= $form->field($model, 'email')->textInput(['minlength' => 6, 'maxlength' => 255]) ?>

    <?= $form->field($model, 'foto')->widget(FileInput::classname(), [
        'data' => $model->foto,
        'options' => ['multiple' => true],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
