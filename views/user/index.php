<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index box box-primary">

    <div class="box-header">
        <h3 class="box-title">Daftar User.</h3>
    </div>

    <div class="box-body table-responsive">

        <?php /*<h1><?= Html::encode($this->title) ?></h1>*/ ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?php /*<?= Html::a('<i class="fa fa-plus"> Tambah User</i>', ['create'], ['class' => 'btn btn-success']) ?>*/ ?>
            <?= Html::a('<i class="fa fa-print"> Export Daftar User ke Word</i>', ['daftar-user-word'], ['class' => 'btn btn-info btn-flat']) ?>
        </p>

        <?php /*
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                'username',
                //'password',
                //'id_anggota',
                //'id_petugas',
                //'id_user_role',
                [  
                    'attribute' => 'id_user_role',
                    'value' => function($data)
                    {
                        return $data->getUserRole();
                    }
                ],
                //'status',
                //'token',

                //['class' => 'yii\grid\ActionColumn'],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {resetpassword} {changepassword}',
                    'buttons' => [
                        'resetpassword' => function($url, $model, $key) {
                            return Html::a('<i class="fa fa-refresh"></i>', ['reset-password', 'id' => $model->id], ['data' => ['confirm' => 'Apa anda yakin ingin me reset password akun ini?'],]);
                        },
                        'changepassword' => function($url, $model, $key) {
                            return Html::a('<i class="fa fa-key"></i>', ['change-password', 'id' => $model->id]);
                        }
                    ]
                ],
            ],
        ]); ?>
        */ ?>

        <?php
            $gridColumns = [
                ['class' => 'kartik\grid\SerialColumn',
                    //'width'=>'5%'
                ],
                [
                    'attribute' => 'username',
                    'label' => 'Username',
                    'vAlign' => 'middle',
                    'value' => function ($model) {
                        return $model->username;
                    },
                ],
                [
                    'label' => 'Status User',
                    'vAlign' => 'middle',
                    'value' => function($model) {
                        return $model->getUserRole();
                    }
                ],
                //['class' => 'yii\grid\ActionColumn'],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {resetpassword} {changepassword}',
                    'buttons' => [
                        'resetpassword' => function($url, $model, $key) {
                            return Html::a('<i class="fa fa-refresh"></i>', ['reset-password', 'id' => $model->id], ['data' => ['confirm' => 'Apa anda yakin ingin me reset password akun ini?'],]);
                        },
                        'changepassword' => function($url, $model, $key) {
                            return Html::a('<i class="fa fa-key"></i>', ['change-password', 'id' => $model->id]);
                        }
                    ]
                ],
            ];

            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns,
                'containerOptions' => ['style' => 'overflow: auto'], 
                'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                'summary'=>'',
                'containerOptions' => ['style'=>'overflow: auto'], 
                'beforeHeader'=>[
                    [
                        'columns'=>[
                            ['content'=> $this->title, 'options'=>['colspan'=>12, 'class'=>'text-center warning']],
                        ],
                        'options'=>['class'=>'skip-export'] 
                    ]
                ],
                'exportConfig' => [
                      GridView::PDF => ['label' => 'Save as PDF'],
                      //GridView::TEXT => ['label' => 'Save as TEXT'],
                      GridView::EXCEL => ['label' => 'Save as EXCEL'],
                      //GridView::HTML => ['label' => 'Save as HTML'],
                      GridView::CSV => ['label' => 'Save as CSV'],
                  ],
                  
                'toolbar' =>  [
                    '{export}', 
                    //'{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                ],
                'pjax' => true,
                'bordered' => true,
                'striped' => true,
                'condensed' => false,
                'responsive' => false,
                'hover' => true,
                'floatHeader' => true,
                'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY
                ],

            ]);
        ?>

    </div>

</div>
