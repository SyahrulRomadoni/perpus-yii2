-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 02, 2019 at 10:08 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `perpus_yii2`
--

-- --------------------------------------------------------

--
-- Table structure for table `anggota`
--

CREATE TABLE `anggota` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `telepon` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `status_aktif` int(11) DEFAULT NULL,
  `foto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anggota`
--

INSERT INTO `anggota` (`id`, `nama`, `alamat`, `telepon`, `email`, `status_aktif`, `foto`) VALUES
(8, 'anggota1', 'ala anggota11', '1242142353254', 'anggota1@gmail.com', 1, ''),
(9, 'anggota2', 'ala anggota2', '4235423424235', 'anggota2@gmail.com', 1, ''),
(13, 'syahrul', 'ala syahrul', '9071049702840', 'syahrulromadoni9898@gmail.com', 1, ''),
(16, 'romadoni', 'ala romadoni', '3532524352523', 'syahrulromadoni8989@gmail.com', 1, '1540453933_Koala.jpg'),
(17, 'aasr01', 'aasr01', '0934729057239', 'aasr01@gmai.com', 1, '1540459101_Penguins.jpg'),
(18, 'aasr02', 'aasr02', '2357239852735', 'aasr02@gmail.com', 1, '1540460007_1.png'),
(19, 'mrsystem', 'mrsystem', '1413532512341', 'mrsystem@gmail.com', 1, '1557541428_14.png'),
(20, 'mrsyahrul', 'mrsyahrul', '1534124242141', 'mrsyahrul@gmail.com', 1, '1557544915_15.png');

-- --------------------------------------------------------

--
-- Table structure for table `buku`
--

CREATE TABLE `buku` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tahun_terbit` year(4) DEFAULT NULL,
  `id_penulis` int(11) DEFAULT NULL,
  `id_penerbit` int(11) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `sinopsis` text DEFAULT NULL,
  `sampul` varchar(255) DEFAULT NULL,
  `berkas` varchar(255) DEFAULT NULL,
  `harga` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buku`
--

INSERT INTO `buku` (`id`, `nama`, `tahun_terbit`, `id_penulis`, `id_penerbit`, `id_kategori`, `sinopsis`, `sampul`, `berkas`, `harga`) VALUES
(12, 'Aku adalah istrimu', 2018, 1, 2, 1, '<p>asdasd asd asd aaasd asd&nbsp;asdasd asd asd aaasd asd&nbsp;asdasd asd asd aaasd asd&nbsp;asdasd asd asd aaasd asd&nbsp;asdasd asd asd aaasd asd&nbsp;asdasd asd asd aaasd asd&nbsp;asdasd asd asd aaasd asd&nbsp;asdasd asd asd aaasd asd&nbsp;asdasd asd asd aaasd asd&nbsp;asdasd asd asd aaasd asd&nbsp;asdasd asd asd aaasd asd&nbsp;asdasd asd asd aaasd asd&nbsp;asdasd asd asd aaasd asd&nbsp;asdasd asd asd aaasd asd&nbsp;asdasd asd asd aaasd asd&nbsp;asdasd asd asd aaasd asd&nbsp;asdasd asd asd aaasd asd&nbsp;asdasd asd asd aaasd asd&nbsp;asdasd asd asd aaasd asd&nbsp;asdasd asd asd aaasd asd&nbsp;</p>', '1534306210_Koala.jpg', '1534306210_semple1.docx', '50000'),
(13, 'Bebaskan aku dari jeratan cintahmu', 2017, 2, 1, 2, '<p>kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;</p>', '1534306248_Penguins.jpg', '1534306248_semple2.docx', '60000'),
(14, 'Cintai aku apa adanya', 2016, 3, 2, 3, '<p>kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;</p>', '1534306301_Chrysanthemum.jpg', '1534306301_semple1.docx', '70000'),
(15, 'Dimanakah dirimu berada', 2015, 1, 1, 4, '<p>kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;kaskad akdhakdhasd askdhaklsdasl askdalkda askdaskd&nbsp;</p>', '1534306335_Hydrangeas.jpg', '1534306335_semple2.docx', '80000'),
(16, 'Jadikan aku malaikat cintahmu', 2018, 2, 1, 19, '<p>jasldkajskd jaksdnasda ndkasd asdaskda shdashdashdu ashdashduans kdashduas ndaskdjnas hsadnkash uasndkasduasnkdn asiuhdas&nbsp;jasldkajskd jaksdnasda ndkasd asdaskda shdashdashdu ashdashduans kdashduas ndaskdjnas hsadnkash uasndkasduasnkdn asiuhdas&nbsp;jasldkajskd jaksdnasda ndkasd asdaskda shdashdashdu ashdashduans kdashduas ndaskdjnas hsadnkash uasndkasduasnkdn asiuhdas&nbsp;jasldkajskd jaksdnasda ndkasd asdaskda shdashdashdu ashdashduans kdashduas ndaskdjnas hsadnkash uasndkasduasnkdn asiuhdas oiajsdoasidjasoidjoiajsdo aiosdhasodnao</p>', '1536806569_Desert.jpg', '1536806569_semple1.docx', '90000'),
(17, 'Zona Cintah', 2015, 3, 2, 17, '<p>jasldkajskd jaksdnasda ndkasd asdaskda shdashdashdu ashdashduans kdashduas ndaskdjnas hsadnkash uasndkasduasnkdn asiuhdas&nbsp;jasldkajskd jaksdnasda ndkasd asdaskda shdashdashdu ashdashduans kdashduas ndaskdjnas hsadnkash uasndkasduasnkdn asiuhdas&nbsp;jasldkajskd jaksdnasda ndkasd asdaskda shdashdashdu ashdashduans kdashduas ndaskdjnas hsadnkash uasndkasduasnkdn asiuhdas&nbsp;jasldkajskd jaksdnasda ndkasd asdaskda shdashdashdu ashdashduans kdashduas ndaskdjnas hsadnkash uasndkasduasnkdn asiuhdas oiajsdoasidjasoidjoiajsdo aiosdhasodnao</p>', '1536806627_Lighthouse.jpg', '1536806627_semple2.docx', '100000'),
(18, 'Hilangkan Lah rasa cintah mu', 2017, 1, 1, 18, '<p>jasldkajskd jaksdnasda ndkasd asdaskda shdashdashdu ashdashduans kdashduas ndaskdjnas hsadnkash uasndkasduasnkdn asiuhdas&nbsp;jasldkajskd jaksdnasda ndkasd asdaskda shdashdashdu ashdashduans kdashduas ndaskdjnas hsadnkash uasndkasduasnkdn asiuhdas&nbsp;jasldkajskd jaksdnasda ndkasd asdaskda shdashdashdu ashdashduans kdashduas ndaskdjnas hsadnkash uasndkasduasnkdn asiuhdas&nbsp;jasldkajskd jaksdnasda ndkasd asdaskda shdashdashdu ashdashduans kdashduas ndaskdjnas hsadnkash uasndkasduasnkdn asiuhdas oiajsdoasidjasoidjoiajsdo aiosdhasodnao</p>', '1536806694_Jellyfish.jpg', '1536806694_semple1.docx', '110000'),
(19, 'Kekalkan lah diriku ini', 2018, 1, 2, 1, '<p>jasldkajskd jaksdnasda ndkasd asdaskda shdashdashdu ashdashduans kdashduas ndaskdjnas hsadnkash uasndkasduasnkdn asiuhdas&nbsp;jasldkajskd jaksdnasda ndkasd asdaskda shdashdashdu ashdashduans kdashduas ndaskdjnas hsadnkash uasndkasduasnkdn asiuhdas&nbsp;jasldkajskd jaksdnasda ndkasd asdaskda shdashdashdu ashdashduans kdashduas ndaskdjnas hsadnkash uasndkasduasnkdn asiuhdas&nbsp;jasldkajskd jaksdnasda ndkasd asdaskda shdashdashdu ashdashduans kdashduas ndaskdjnas hsadnkash uasndkasduasnkdn asiuhdas oiajsdoasidjasoidjoiajsdo aiosdhasodnao</p>', '1536806749_Tulips.jpg', '1536806749_semple2.docx', '120000'),
(20, 'Dirumu ada Diriku', 2018, 2, 3, 10, '<p>jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk&nbsp;jkasdhkasd alskdhasl asdsdlk</p>', '1538107154_Penguins.jpg', '1538106719_semple1.docx', '130000'),
(21, 'Istriku adalah anak ku', 2018, 1, 1, 2, '<p>aasdas sd asd sad sad as dsa dsa dsad sad sadsaos dasj doisa jdiosa djaosdjsaiod saoiduasiodj waodijas iodja osidjas odijasd&nbsp;aasdas sd asd sad sad as dsa dsa dsad sad sadsaos dasj doisa jdiosa djaosdjsaiod saoiduasiodj waodijas iodja osidjas odijasd&nbsp;aasdas sd asd sad sad as dsa dsa dsad sad sadsaos dasj doisa jdiosa djaosdjsaiod saoiduasiodj waodijas iodja osidjas odijasd&nbsp;aasdas sd asd sad sad as dsa dsa dsad sad sadsaos dasj doisa jdiosa djaosdjsaiod saoiduasiodj waodijas iodja osidjas odijasd&nbsp;aasdas sd asd sad sad as dsa dsa dsad sad sadsaos dasj doisa jdiosa djaosdjsaiod saoiduasiodj waodijas iodja osidjas odijasd&nbsp;aasdas sd asd sad sad as dsa dsa dsad sad sadsaos dasj doisa jdiosa djaosdjsaiod saoiduasiodj waodijas iodja osidjas odijasd&nbsp;</p>', '1540352183_1.png', '1540352183_semple1.docx', '140000'),
(22, 'Anak ku adalah istriku', 2018, 2, 3, 5, '<p>lskdasj lksajdlaksjdslakd jaslk jdlkasjd lsakjdalsk jd&nbsp;lskdasj lksajdlaksjdslakd jaslk jdlkasjd lsakjdalsk jd&nbsp;lskdasj lksajdlaksjdslakd jaslk jdlkasjd lsakjdalsk jd&nbsp;lskdasj lksajdlaksjdslakd jaslk jdlkasjd lsakjdalsk jd&nbsp;lskdasj lksajdlaksjdslakd jaslk jdlkasjd lsakjdalsk jd&nbsp;lskdasj lksajdlaksjdslakd jaslk jdlkasjd lsakjdalsk jd&nbsp;lskdasj lksajdlaksjdslakd jaslk jdlkasjd lsakjdalsk jd&nbsp;lskdasj lksajdlaksjdslakd jaslk jdlkasjd lsakjdalsk jd&nbsp;</p>', '1540354672_2.png', '1540354672_semple2.docx', '150000'),
(23, 'Pacar ku di bawa lari orang lain', 2010, 3, 2, 5, '<p>fassadsadasdsadsa asdsa sadasd asd asd sad asd asdasdasdasdj aisuhdkjsa hdjkahsdkasbdjkahsdiujasndkj</p>', '1563713281_01.png', '1563713281_AA.docx', '50000');

-- --------------------------------------------------------

--
-- Table structure for table `denda`
--

CREATE TABLE `denda` (
  `id` int(11) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `denda`
--

INSERT INTO `denda` (`id`, `id_peminjaman`, `harga`) VALUES
(1, 28, 66000),
(2, 29, 2000),
(3, 30, 123000),
(4, 33, 147000),
(5, 37, 0),
(6, 38, 0),
(7, 35, 84000),
(8, 36, 84000),
(9, 34, 161000),
(10, 31, 180000),
(11, 32, 180000);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama`) VALUES
(1, 'Agama'),
(2, 'Pendidikan'),
(3, 'Cerpen'),
(4, 'Puisi'),
(5, 'Novel'),
(6, 'Elektronik'),
(7, 'Cergam'),
(8, 'Komik'),
(9, 'Ensiklopedi'),
(10, 'Nomik'),
(11, 'Antologi'),
(12, 'Dongeng'),
(13, 'Biografi'),
(14, 'Jurnal'),
(15, 'Novelet'),
(16, 'Fotografi'),
(17, 'Karya ilmiah'),
(18, 'Tafsir'),
(19, 'Kamus'),
(20, 'Panduan'),
(21, 'Atlas'),
(22, 'Ilmiah'),
(23, 'Teks'),
(24, 'Mewarnai');

-- --------------------------------------------------------

--
-- Table structure for table `kenaikan_denda`
--

CREATE TABLE `kenaikan_denda` (
  `id` int(11) NOT NULL,
  `hari` int(11) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kenaikan_denda`
--

INSERT INTO `kenaikan_denda` (`id`, `hari`, `harga`) VALUES
(1, 1, 1000);

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id` int(11) NOT NULL,
  `id_buku` int(11) NOT NULL,
  `id_anggota` int(11) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_buku` int(11) NOT NULL,
  `tanggal_pengembalian_buku` date NOT NULL,
  `harga` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peminjaman`
--

INSERT INTO `peminjaman` (`id`, `id_buku`, `id_anggota`, `tanggal_pinjam`, `tanggal_kembali`, `status_buku`, `tanggal_pengembalian_buku`, `harga`) VALUES
(28, 12, 13, '2018-11-21', '2018-11-28', 2, '2019-02-02', 66000),
(29, 13, 13, '2019-01-31', '2019-02-06', 2, '2019-02-08', 2000),
(30, 14, 13, '2018-10-01', '2018-10-08', 2, '2019-02-08', 123000),
(31, 15, 16, '2018-11-04', '2018-11-11', 2, '2019-05-10', 180000),
(32, 16, 16, '2018-11-04', '2018-11-11', 2, '2019-05-10', 180000),
(33, 17, 16, '2018-11-04', '2018-11-11', 2, '2019-04-07', 147000),
(34, 12, 17, '2018-11-23', '2018-11-30', 0, '0000-00-00', 0),
(35, 13, 8, '2019-02-08', '2019-02-15', 2, '2019-05-10', 84000),
(36, 15, 8, '2019-02-08', '2019-02-15', 2, '2019-05-10', 84000),
(37, 17, 13, '2019-04-07', '2019-04-14', 2, '2019-04-07', 0),
(38, 19, 13, '2019-05-10', '2019-05-17', 0, '0000-00-00', 0),
(39, 20, 20, '2019-05-11', '2019-05-18', 0, '0000-00-00', 0),
(40, 19, 17, '2019-05-28', '2019-06-04', 0, '0000-00-00', 0),
(41, 23, 17, '2019-12-02', '2019-12-09', 1, '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `penerbit`
--

CREATE TABLE `penerbit` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `telepon` varchar(255) DEFAULT NULL,
  `email` varchar(2555) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penerbit`
--

INSERT INTO `penerbit` (`id`, `nama`, `alamat`, `telepon`, `email`) VALUES
(1, 'Polindra', 'Jl. Kebenaran 104', '087733195026', 'polindra@gmail.com'),
(2, 'PT. Syahrul', 'Jl. Kebenaran 303', '087733195025', 'pt.syahrul@gmail.com'),
(3, 'Senang Hati', 'Dimana-mana aja yang penting happy', '0912471209471', 'senanghati@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `penulis`
--

CREATE TABLE `penulis` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` text DEFAULT NULL,
  `telepon` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penulis`
--

INSERT INTO `penulis` (`id`, `nama`, `alamat`, `telepon`, `email`) VALUES
(1, 'Syahrul', 'Jl. Kebenaran 101', '087733195029', 'syahrul@gmail.com'),
(2, 'Samsul', 'Jl. Kebenaran 102', '087733195028', 'samsul@gmail.com'),
(3, 'Maul', 'Jl. Kebenaran 103', '087733195027', 'maul@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `perubahan_denda`
--

CREATE TABLE `perubahan_denda` (
  `id` int(11) NOT NULL,
  `id_kenaikan_denda` int(11) NOT NULL,
  `harga` varchar(255) NOT NULL,
  `tanggal_perubahan` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `perubahan_denda`
--

INSERT INTO `perubahan_denda` (`id`, `id_kenaikan_denda`, `harga`, `tanggal_perubahan`) VALUES
(1, 1, '1500', '2019-11-18'),
(2, 1, '1000', '2019-11-18'),
(3, 1, '1500', '2019-12-02'),
(4, 1, '1000', '2019-12-02');

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `telepon` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `foto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id`, `nama`, `alamat`, `telepon`, `email`, `foto`) VALUES
(4, 'petugas1', 'ala petugas1', '2312321321321', 'petugas1@gmail.com', ''),
(5, 'petugas2', 'ala petugas2', '1232134243254', 'petugas2@gmail.com', ''),
(6, 'petugas3', 'petugas3', '9017503759027', 'petugas3@gmail.com', '1540462069_Chrysanthemum.jpg'),
(7, 'petugas4', 'ala petugas4', '8375289323253', 'petugas4@gmail.com', '1542427864_Hydrangeas.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_anggota` int(11) DEFAULT 0,
  `id_petugas` int(11) DEFAULT 0,
  `id_user_role` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `token` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `id_anggota`, `id_petugas`, `id_user_role`, `status`, `token`) VALUES
(1, 'admin', '$2y$13$MSTQJKYv9O9Cmhs0izn3wO0hlso23fk1ucW8Iu0/iDK4ivS32oqjy', 0, 0, 1, 1, ''),
(14, 'anggota1', '$2y$13$zpVYASJCqEZAwDKUQISxBuLYuJN4tM2Sr/H04bCDc5h21Jkw4.Wn.', 8, 0, 2, 2, ''),
(16, 'petugas1', '$2y$13$AmXBSZfLMlqZP1brszsSO.scYoy1F3/lvKMl.K/F53uOmbV2z6mMu', 0, 4, 3, 3, ''),
(17, 'petugas2', '$2y$13$WSHxM9zUDfQCCNtSugO7Yu6Pu7kboic4AOvckpnVUFIKZ3jurNWNe', 0, 5, 3, 3, ''),
(18, 'syahrul', '$2y$13$pHsVdz83TAfUKZCB56PK/e.z.OXH.H911UEkFTr4l4cFAQuEqPi.i', 13, 0, 2, 2, '2T6jBaYoA6hhz27yo0MU-MhA8sg6RBKJL4tJYd9pC35R_bgYOX'),
(21, 'romadoni', '$2y$13$S.qIeJw/AbVRgqYRw09ZzuFs5yZcYjYBPtx.Fhu8otuYmbWwyYeAe', 16, 0, 2, 2, 'gsPAWIvoTmDVyCuJhfMk0mTWNQMAtvi7wV8-yszn41UjLu_59T'),
(22, 'aasr01', '$2y$13$7lx4IGC/luxZekOgDR89UuTr9K1I5toTYtrMEz05fsdGXgO.1jZre', 17, 0, 2, 2, 'M3JZV6H1cYF8ZJD1lUXKkNFmK37BTuOjsblE0RrkNlYlVXG_UN'),
(23, 'aasr02', '$2y$13$RzwHBmOh/RIC1YLFigIepOJGPyrVqBqa8sKyPnFO98j39gjIWIEK2', 18, 0, 2, 2, 'A2EQeRwBiNy5jkbYa_YaBHmsjo2zS5CqULc6LJvhxnero8Y-gb'),
(24, 'petugas3', '$2y$13$rqIsdURrZUT2G74Wm9DsxeFDmkC9dZFdM4FOqsc5b6LT.Q/urzhzK', 0, 6, 3, 3, 'rngsnLAs7kVv6JQ8oT49q1Hum3kKCOclbsqHwuDs_c4qnGtpx7'),
(25, 'petugas4', '$2y$13$561U6Kctv0mwABMNDK2tve29Xk9A.PJAl/NhElz7wRn2TBiuFVpJu', 0, 7, 3, 3, 'DuGgS4aF4dMv1QSY61Ca-fFWYdK2N1k0UKTtC66b7IaNCQznOQ'),
(26, 'mrsystem', '$2y$13$z.Uk6p6Z7O9gyeF50og37e0HiW8g7N7/OkbWYq1BsSAxdgHi66vX.', 19, 0, 2, 2, 'NT-foAFmsv1RZ_JuHtku3rmylKPxQv4M4zwIkmATsq0vkKPjte'),
(27, 'mrsyahrul', '$2y$13$cLzeSEnJuFWRdXP9rbysTuW/5WyZ9X2bIj5mh.utsJKQuse5asJpm', 20, 0, 2, 2, 'V9n1BusOMGYi8nR8JLmFTw1ONbZWOpZAz3NM_nk_dRkLKQKBro');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `nama`) VALUES
(1, 'Admin'),
(2, 'Anggota'),
(3, 'Petugas');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `denda`
--
ALTER TABLE `denda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kenaikan_denda`
--
ALTER TABLE `kenaikan_denda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penerbit`
--
ALTER TABLE `penerbit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penulis`
--
ALTER TABLE `penulis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perubahan_denda`
--
ALTER TABLE `perubahan_denda`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anggota`
--
ALTER TABLE `anggota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `buku`
--
ALTER TABLE `buku`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `denda`
--
ALTER TABLE `denda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `kenaikan_denda`
--
ALTER TABLE `kenaikan_denda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `penerbit`
--
ALTER TABLE `penerbit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `penulis`
--
ALTER TABLE `penulis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `perubahan_denda`
--
ALTER TABLE `perubahan_denda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
