<?php

namespace app\controllers;

use Yii;
use app\models\Peminjaman;
use app\models\PeminjamanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;
use yii\filters\AccessControl;
use app\models\Denda;
use app\models\KenaikanDenda;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Shared\Converter;

/**
 * PeminjamanController implements the CRUD actions for Peminjaman model.
 */
class PeminjamanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [

            // Access Control URL.
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['update', 'view', 'delete', 'kembalikan-buku', 'daftar-peminjaman-buku-word'],
                        'allow' => User::isAdmin() || User::isPetugas(),
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index', 'create', 'cek-status'],
                        'allow' => User::isAdmin() || User::isPetugas() || User::isAnggota(),
                        'roles' => ['@'],
                    ],
                ],
            ],

            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Peminjaman models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PeminjamanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Peminjaman model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Peminjaman model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_buku = null)
    {
        $model = new Peminjaman();
        $model->id_buku = $id_buku;
        $model->status_buku = 1;
        $model->tanggal_kembali = date('Y-m-d', strtotime('+7 days'));
        $model->tanggal_pengembalian_buku = '0000-00-00';
        $model->harga = 0;

        // Jika anggota meminjam buku maka auto crud
        if (Yii::$app->user->identity->id_user_role == 2) {
            $model->id_anggota = Yii::$app->user->identity->id_anggota;
            $model->tanggal_pinjam = date('Y-m-d');
            $model->tanggal_kembali = date('Y-m-d', strtotime('+7 days'));
            $model->status_buku = 1;
            $model->tanggal_pengembalian_buku = '0000-00-00';
            $model->harga = 0;

            // Yii::$app->mail->compose('@app/template/pemberitahuanemail',['model' => $model])
            //     ->setFrom('syahrulromadoni8899@gmail.com')
            //     ->setTo($model->anggota->email)
            //     ->setSubject('Peminjaman Buku')
            //     ->send();

            $model->save();
            return $this->redirect(['index']); 
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Peminjaman model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Peminjaman model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Peminjaman model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Peminjaman the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Peminjaman::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    // Resert password anggota yang bisa di pakai oleh admin dan petugas.
    public function actionKembalikanBuku($id)
    {
        $model = Peminjaman::findOne($id);
        
        $model->status_buku = 2;
        $model->tanggal_pengembalian_buku = date('Y-m-d');

        $selisih = $model->getSelisih();

        $denda1 = new Denda();
        $denda1->id_peminjaman = $model->id;

        /*if (KenaikanDenda::getDenda(1) <= $selisih) {
            //Selisih 1
            $model->harga 
        } elseif (KenaikanDenda::getDenda(2) <= $selisih) {
            //Selisih 2
        } elseif (KenaikanDenda::getDenda(3) <= $selisih) {
            //Selisih 3
        } elseif (KenaikanDenda::getDenda(4) <= $selisih) {
            //Selisih 4
        } elseif (KenaikanDenda::getDenda(5) <= $selisih) {
            //Selisih 5
        } elseif (KenaikanDenda::getDenda(6) <= $selisih) {
            //Selisih 6
        } else {
            //Selisih 7
        }*/
        
        // Denda static ikut di databses
        /*foreach (\app\models\KenaikanDenda::find()->all() as $denda) {
            if ($denda->hari <= $selisih) {
                $model->harga = $denda->harga;
                $denda1->harga = $denda->harga;
            }
        }*/

        // Denda Dinamis di atur di codingan
        /*if ($model->tanggal_kembali <= date('Y-m-d')) {
            $model->harga = 1500*$selisih;
            $denda1->harga = 1500*$selisih;
        }*/

        // Denda dinamis usah relasi dengan kenaikan denda di database
        foreach (\app\models\KenaikanDenda::find()->all() as $denda) {
            if ($model->tanggal_kembali <= date('Y-m-d')) {
                $model->harga = $denda->harga*$selisih;
                $denda1->harga = $denda->harga*$selisih;
            }
        }

        $model->save(false);
        $denda1->save(false);

        Yii::$app->session->setFlash('Berhasil', 'Buku sudah berhasil di kembalikan');
        
        if (User::isAdmin()) {
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            return $this->redirect(['peminjaman/index']);
        }
    }

    public function actionCekStatus()
    {
        $query = Peminjaman::find()
            ->andWhere(['<','tanggal_kembali',date('Y-m-d')])
            ->andWhere(['tanggal_pengembalian_buku' => null])
            ->all();

        /*
        $query = Peminjaman::find()
            ->andWhere(['tanggal_kembali' => date('Y-m-d')])
            ->all();
        */

        foreach ($query as $peminjaman) {
            $peminjaman->status_buku = Peminjaman::DIKEMBALIKAN;
            $peminjaman->save();
        }

        // return $this->goBack();
    }

    public function actionDaftarPeminjamanBukuWord()
    {
        // Membuat model baru
        $phpWord = new PhpWord();

        // Membuat default ukuran fontz
        $phpWord->setDefaultFontSize(11);

        // Membuat default fontz
        $phpWord->setDefaultFontName('Gentium Basic');

        // Membuat Jarak kertasnya
        $section = $phpWord->addSection([
            'marginTop' => Converter::cmToTwip(1.50),
            'marginBottom' => Converter::cmToTwip(1.50),
            'marginLeft' => Converter::cmToTwip(1.2),
            'marginRight' => Converter::cmToTwip(1.2),
        ]);

        // Custom Style
        $headerStyle = [
            'bold' => true,
        ];

        $paragraphCenter = [
            'alignment' => 'center',
            'spacing' => 0,
        ];

        // Mulai
        // Label atas, tengah
        $section->addText(
            'DAFTAR PEMINAJAMAN BUKU',
            $headerStyle,
            $paragraphCenter
        );

        $section->addText(
            'Daftar Peminjaman Buku Perpustakaan Yii2',
            $headerStyle,
            $paragraphCenter
        );

        // Breack
        $section->addTextBreak(1);

        // Label samping kiri
        // $section->addText(
        //     'PEJABAT PENGADAAN BARANG/JASA',
        //     $headerStyle,
        //     [
        //         'alignment' => 'left'
        //     ]
        // );

        // $section->addText(
        //     'SATKER 450417 LAN JAKARTA',
        //     $headerStyle,
        //     [
        //         'alignment' => 'left'
        //     ]
        // );

        // Breack
        //$section->addTextBreak(1);

        // Label yang di tengah
        // $section->addText(
        //     'PEKERJAAN PEMBANGUNAN SISTEM INFORMASI PENGADAAN (SIP) KANTOR LAN JAKARTA ',
        //     $headerStyle,
        //     $paragraphCenter
        // );

        // Breack
        //$section->addTextBreak(1);

        // Label di samping
        // $section->addText(
        //     'PAGU DANA  :   Rp. 12.000.000,-',
        //     $headerStyle,
        //     [
        //         'alignment' => 'left'
        //     ]
        // );

        // $section->addText(
        //     'HPS       : Rp. 11.000.000,- ',
        //     $headerStyle,
        //     [
        //         'alignment' => 'left'
        //     ]
        // );

        // Table
        $table = $section->addTable([
            'alignment' => 'center', 
            'bgColor' => '000000',
            'borderSize' => 6,
        ]);

        // Row
        $table->addRow(null);
        $table->addCell(500)->addText('No', $headerStyle, $paragraphCenter);
        $table->addCell(1500)->addText('Nama Buku', $headerStyle, $paragraphCenter);
        $table->addCell(1500)->addText('Nama Peminjaman', $headerStyle, $paragraphCenter);
        $table->addCell(1500)->addText('Tanggal Pinjam', $headerStyle, $paragraphCenter);
        $table->addCell(1500)->addText('Tanggal Kembali', $headerStyle, $paragraphCenter);
        $table->addCell(1500)->addText('Status Buku', $headerStyle, $paragraphCenter);
        $table->addCell(1500)->addText('Telat', $headerStyle, $paragraphCenter);
        $table->addCell(1500)->addText('Tanggal Pengembalian Buku', $headerStyle, $paragraphCenter);
        $table->addCell(1500)->addText('Denda', $headerStyle, $paragraphCenter);

        $semuaPeminjaman = Peminjaman::find()->all();
        $nomor = 1;

        // Perulangan
        foreach ($semuaPeminjaman as $peminjaman)
        {
            $table->addRow(null);
            $table->addCell(500)->addText($nomor++, null, $paragraphCenter);
            $table->addCell(1500)->addText($peminjaman->buku->nama, null, $paragraphCenter);
            $table->addCell(1500)->addText($peminjaman->anggota->nama, null, $paragraphCenter);
            $table->addCell(1500)->addText($peminjaman->tanggal_pinjam, null, $paragraphCenter);
            $table->addCell(1500)->addText($peminjaman->tanggal_kembali, null, $paragraphCenter);
            $table->addCell(1500)->addText($peminjaman->getStatusPeminjaman(), null, $paragraphCenter);
            $table->addCell(1500)->addText($peminjaman->getTelat(), null, $paragraphCenter);
            $table->addCell(1500)->addText($peminjaman->tanggal_pengembalian_buku, null, $paragraphCenter);
            $table->addCell(1500)->addText($peminjaman->harga, null, $paragraphCenter);
        }

        $section->addTextBreak(1);

        $denda = Peminjaman::find()->sum('harga');
        $section->addText(
            'Jumlah total denda Peminjaman : Rp.' . $denda,
            $paragraphCenter
        );

        // Tempat penyimpanan file sama nama file.
        $filename = time() . '_' . 'Daftar-Peminjaman-Buku.docx';
        $path = 'document/' . $filename;
        $xmlWrite = IOFactory::createWriter($phpWord, 'Word2007');
        $xmlWrite->save($path);

        return $this->redirect($path);
    }
}
