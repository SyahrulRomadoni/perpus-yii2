<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "perubahan_denda".
 *
 * @property int $id
 * @property int $id_kenaikan_denda
 * @property string $harga
 * @property string $tanggal_perubahan
 */
class PerubahanDenda extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'perubahan_denda';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kenaikan_denda', 'harga', 'tanggal_perubahan'], 'required'],
            [['id_kenaikan_denda'], 'integer'],
            [['tanggal_perubahan'], 'safe'],
            [['harga'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_kenaikan_denda' => 'Id Kenaikan Denda',
            'harga' => 'Harga',
            'tanggal_perubahan' => 'Tanggal Perubahan',
        ];
    }
}
