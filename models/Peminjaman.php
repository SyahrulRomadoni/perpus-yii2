<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "peminjaman".
 *
 * @property int $id
 * @property int $id_buku
 * @property int $id_anggota
 * @property string $tanggal_pinjam
 * @property string $tanggal_kembali
 */
class Peminjaman extends \yii\db\ActiveRecord
{
    const DIKEMBALIKAN = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'peminjaman';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_buku', 'id_anggota', 'tanggal_pinjam', 'tanggal_kembali', 'status_buku', 'tanggal_pengembalian_buku'], 'required'],
            [['id_buku', 'id_anggota', 'status_buku', 'harga'], 'integer'],
            [['tanggal_pinjam', 'tanggal_kembali', 'tanggal_pengembalian_buku'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_buku' => 'Buku',
            'id_anggota' => 'Nama Anggota',
            'tanggal_pinjam' => 'Tanggal Pinjam',
            'tanggal_kembali' => 'Tanggal Kembali',
            'status_buku' => 'Status Buku',
            'tanggal_pengembalian_buku' => 'Tanggal Pengembalian Buku',
            'harga' => 'Denda',
        ];
    }

    // Untuk menghitung jumlah data yang ada di tabel ini sendiri dan di tampilkan chart kotak.
    public static function getCount()
    {
        return static::find()->count();
    }

    // Untuk mengambil id_anggota di tabel peminjaman dirubah jadi nama anggota yang ada di tabel anggota yang akan di muncukan di index peminjaman
    public function getAnggota()
    {
        // Cara 1 mendapatkan id_*** menjadi nama.
        // $model = Anggota::findOne($this->id_anggota);

        // if ($model !== null) {
        //     return $model->nama;
        // } else {
        //     return null;
        // }

        // Cara 2 mendapatkan id_*** menjadi nama.
        return $this->hasOne(Anggota::className(), ['id' => 'id_anggota']);
    }

    // Untuk mengambil id_buku di tabel buku dirubah jadi nama buku yang ada di tabel buku yang akan di muncukan di index peminjaman
    public function getBuku()
    {
        // Cara 1 mendapatkan id_*** menjadi nama.
        // $model = Buku::findOne($this->id_buku);

        // if ($model !== null) {
        //     return $model->nama;
        // } else {
        //     return null;
        // }

        // Cara 2 mendapatkan id_*** menjadi nama.
        return $this->hasOne(Buku::className(), ['id' => 'id_buku']);
    }

    public static function getListBulanGrafik()
    {
        $list = [];

        for ($i=1; $i <= 12 ; $i++) {
            $list[] = self::getBulanSingkat($i);
        }

        return $list;
    }
                            
    public static function getCountGrafik()
    {
        $list = [];
        for ($i = 1; $i <= 12; $i++) {
            if (strlen($i) == 1) $i = '0' . $i;
            $count = static::findCountGrafik($i);

            $list [] = (int)@$count->count();

        }

        return $list;
    }

    public static function findCountGrafik($bulan)
    {
        $tahun = date('Y');
        $lastDay = date("t", strtotime($tahun.'_'.$bulan));

        return static::find()->andWhere(['between','tanggal_pinjam', "$tahun-$bulan-01", "$tahun-$bulan-$lastDay"]);
    }

    public function getStatusPeminjaman()
    {
        if ($this->status_buku === 0) {
            return "Belum Dikembalikan";
        };
        if ($this->status_buku === 1) {
            return "Masih Dipinjam";
        };
        if ($this->status_buku === 2) {
            return "Sudah Dikembalikan";
        };
    }

    public static function getSelisihTanggal($tanggal_pinjam, $tanggal_kembali, $key = 'd')
    {
        $tanggal_pinjam  = date_create($tanggal_pinjam);
        $tanggal_kembali = date_create($tanggal_kembali); /*->modify('+1 day');*/ //Tangal sekarang +1 hari
        
        $diff  = date_diff($tanggal_pinjam, $tanggal_kembali);
        
        switch ($key) {
            case 'y':
                return $diff->format('%a');
                break;
            case 'm':
                return $diff->format('%a');
                break;
            case 'd':
                return $diff->format('%a');
                break;
            default:
                return $diff->format('%a');
                break;
        }
    }

    public function getSelisih()
    {
        return $this->getSelisihTanggal($this->tanggal_kembali, date('Y-m-d'));
    }

    public function getTelat()
    {
        if ($this->status_buku == 2) {
            return "- Hari";
        };
        if ($this->tanggal_kembali <= date('Y-m-d')) {
            return $this->getSelisih() . " Hari";
        } else {
            return "0 Hari";
        };
    }
}
