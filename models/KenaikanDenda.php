<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kenaikan_denda".
 *
 * @property int $id
 * @property int $hari
 * @property int $harga
 */
class KenaikanDenda extends \yii\db\ActiveRecord
{
    // const SATU = 1;
    // const DUA = 2;
    // const TIGA = 3;
    // const EMPAT = 4;
    // const LIMA = 5;
    // const ENAM = 6;
    // const TUJUH = 7;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kenaikan_denda';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hari', 'harga'], 'required'],
            [['hari', 'harga'], 'integer'],
            [['hari'],'match', 'pattern' => '/^[0-9]\w*$/i','message' => 'Enter Number Only'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hari' => 'Hari',
            'harga' => 'Harga',
        ];
    }

    public static function getDenda($hari)
    {
        return static::findOne(['hari' => $hari])->hari;
    }
}
